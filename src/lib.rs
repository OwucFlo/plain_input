#[derive(Clone, Debug)]
pub enum MouseInput {
    Move([i32; 2], [i32; 2]), //(relative, absolute)
    Button(u8, bool, [i32; 2]),
    Scroll([i32; 2], [i32; 2]), //(scroll, cursor_pos)
    Zoom(i32, [i32; 2])
}

#[derive(Clone, Debug)]
pub enum Input {
    Mouse(MouseInput),
    Text(String)
}
